/* 
 * File:   Body.h
 * Author: fridlik
 *
 * Created on November 5, 2015, 11:00 PM
 */

#ifndef BODY_H
#define	BODY_H
#include "Vector.h"

class Body {
public:
	Body();
	Body(Vector position, Vector velocity, double mass, double time);
	Body(const Body& orig);
	virtual ~Body();

	void move(Vector & F);
	Vector & getPosition();
	Vector computeForceTo(Body &);
	Vector computeForceTo(Body *);
	Vector computeForceTo(Vector &, double mass);

	friend std::ostream & operator<<(std::ostream &, const Body &);

	double getX(){return this->m_position.coordinate(0);}
	double getY(){return this->m_position.coordinate(1);}
	double getMass(){return this->m_mass;}

private:
	Vector m_position;
	Vector m_velocity;
	double m_mass;
	double m_time;
};

#endif	/* BODY_H */

