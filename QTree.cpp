/* 
 * File:   QTree.cpp
 * Author: fridlik
 * 
 * Created on November 11, 2015, 5:03 PM
 */

#include "QTree.h"

QTree::QTree() {
	nw = NULL;
	ne = NULL;
	sw = NULL;
	se = NULL;
	borders = Borders();
	leaf = true;
	item = NULL;

	center_of_mass_acc[0] = 0;
	center_of_mass_acc[1] = 0;
	total_mass = 0;
	childs = 0;
	force = Vector(0, 0);

	pthread_mutex_init(&m_lock, NULL);
}

QTree::QTree(Borders b) {
	nw = NULL;
	ne = NULL;
	sw = NULL;
	se = NULL;
	borders = b;
	leaf = true;
	item = NULL;
	pthread_mutex_init(&m_lock, NULL);
	center_of_mass_acc[0] = 0;
	center_of_mass_acc[1] = 0;
	total_mass = 0;
	childs = 0;
	force = Vector(0, 0);

}

QTree::QTree(const QTree& orig) {
}

QTree::~QTree() {
	if (nw) {
		delete nw;
		delete ne;
		delete sw;
		delete se;
	}

	//	if (item)
	//		delete item;

	pthread_mutex_destroy(&m_lock);
}

void QTree::divide() {
	//	std::cout << "divide" << std::endl;
	double size = borders.half_size / 2;
	Vector v = Vector(borders.center.coordinate(0) - size, borders.center.coordinate(1) - size);
	nw = new QTree(Borders(v, size));

	v = Vector(borders.center.coordinate(0) - size, borders.center.coordinate(1) + size);
	sw = new QTree(Borders(v, size));

	v = Vector(borders.center.coordinate(0) + size, borders.center.coordinate(1) + size);
	se = new QTree(Borders(v, size));

	v = Vector(borders.center.coordinate(0) + size, borders.center.coordinate(1) - size);
	ne = new QTree(Borders(v, size));
	leaf = false;
}

std::ostream & operator<<(std::ostream & os, const QTree & tree) {
	tree.print(os, 0);
	return os;
}

void QTree::print(std::ostream & os, int spaces) const {
	for (int i = 0; i < spaces; i++) {
		os << " ";
	}
	os << "center:" << borders.center << " half_size:" << borders.half_size << " mass:" << mass_center;
	if (!leaf) {
		os << std::endl;
		nw->print(os, spaces + 1);
		ne->print(os, spaces + 1);
		sw->print(os, spaces + 1);
		se->print(os, spaces + 1);
	} else {
		if (item != NULL) {
			os << *item;
		}
		os << std::endl;
	}

}

bool QTree::insert(Body * b, bool lock) {
	if (lock) {
		pthread_mutex_lock(&m_lock);
	}

	if (!borders.contains(b->getX(), b->getY())) {
		if (lock) {
			pthread_mutex_unlock(&m_lock);
		}
		//		std::cout << "not in borders" << *b << std::endl;
		return false;
	}

	center_of_mass_acc[0] += b->getMass() * b->getX();
	center_of_mass_acc[1] += b->getMass() * b->getY();
	total_mass += b->getMass();

	if (leaf && item == NULL) {
		item = b;
		childs++;

		if (lock) {
			pthread_mutex_unlock(&m_lock);
		}
		return true;
	}

	if (nw == NULL) {
		leaf = false;
		divide();
		this->insert(item, false);
		item = NULL;
		center_of_mass_acc[0] -= b->getMass() * b->getX();
		center_of_mass_acc[1] -= b->getMass() * b->getY();
		total_mass -= b->getMass();
	}

	if (lock) {
		pthread_mutex_unlock(&m_lock);
	}

	if (nw->insert(b, true))
		return true;
	if (ne->insert(b, true))
		return true;
	if (sw->insert(b, true))
		return true;
	if (se->insert(b, true))
		return true;

	std::cout << "false" << std::endl;
	return false;
}

QTree* QTree::getChild(int c) {
	switch (c) {
		case 0: return nw;
		case 1: return ne;
		case 2: return sw;
		case 3: return se;
	}
	return NULL;
}

void QTree::computeCenterOfMass() {
	if (!leaf) {
		mass_center = Vector(center_of_mass_acc[0] / total_mass, center_of_mass_acc[1] / total_mass);
	}
	
	if(nw){
		nw->computeCenterOfMass();
		ne->computeCenterOfMass();
		sw->computeCenterOfMass();
		se->computeCenterOfMass();
	}

}

Vector & QTree::getCenterOfMass() {
	return mass_center;
}

Vector QTree::computeForce(Body * b, double theta) {

	if (leaf) {
		if (!item || item == b) {
			return Vector(0, 0);
		} else {
			//			std::cout << "computeForTo" << std::endl;
			return b->computeForceTo(this->item);
			//			return item->computeForceTo(b);
		}

	} else {
		//		std::cout << "else" << std::endl;
		double d = this->borders.half_size * 2;
		double r = this->mass_center.distanceTo(b->getPosition());

		if (d / r < theta) {
			//single body
			return b->computeForceTo(mass_center, total_mass);

		} else {
			return nw->computeForce(b, theta) +
					ne->computeForce(b, theta) +
					sw->computeForce(b, theta) +
					se->computeForce(b, theta);
			
		}
	}




}

int QTree::countItem() {
	if (item)
		return 1;
	if (nw) {
		return nw->countItem() + ne->countItem() + sw->countItem() + se->countItem();
	}
	return 0;
}



