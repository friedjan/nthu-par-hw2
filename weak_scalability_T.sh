#!/bin/bash

TEST_NAME="_weak_T"

FILE="pthread_100_test4${TEST_NAME}"
echo "$FILE"
./nb-pthread 10 1 100 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-pthread 10 1 200 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-pthread 10 1 300 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-pthread 10 1 400 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-pthread 10 1 500 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-pthread 10 1 600 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE



FILE="openmp_100_test4${TEST_NAME}"
echo "$FILE"
./nb-openmp 10 1 100 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-openmp 10 1 200 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-openmp 10 1 300 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-openmp 10 1 400 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-openmp 10 1 500 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE
./nb-openmp 10 1 600 1 samples/test2.txt 0.5 disable -1 -1  2.5 500 >> $FILE



FILE="bh_100_test4_0.1${TEST_NAME}"
echo "$FILE"
./nb-bh 10 1 100 1 samples/test2.txt 0.1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 200 1 samples/test2.txt 0.1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 300 1 samples/test2.txt 0.1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 400 1 samples/test2.txt 0.1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 500 1 samples/test2.txt 0.1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 600 1 samples/test2.txt 0.1 disable -1 -1  2.5 500 >> $FILE



FILE="bh_100_test4_1${TEST_NAME}"
echo "$FILE"
./nb-bh 10 1 100 1 samples/test2.txt 1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 200 1 samples/test2.txt 1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 300 1 samples/test2.txt 1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 400 1 samples/test2.txt 1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 500 1 samples/test2.txt 1 disable -1 -1  2.5 500 >> $FILE
./nb-bh 10 1 600 1 samples/test2.txt 1 disable -1 -1  2.5 500 >> $FILE










