#!/bin/bash

FILE="pthread_100_test4"
echo "$FILE"
echo "1"
./nb-pthread 1 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "2"
./nb-pthread 2 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "3"
./nb-pthread 3 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "4"
./nb-pthread 4 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "5"
./nb-pthread 5 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "6"
./nb-pthread 6 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "7"
./nb-pthread 7 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "8"
./nb-pthread 8 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "9"
./nb-pthread 9 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "10"
./nb-pthread 10 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE





FILE="openmp_100_test4"
echo "$FILE"
echo "1"
./nb-openmp 1 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "2"
./nb-openmp 2 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "3"
./nb-openmp 3 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "4"
./nb-openmp 4 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "5"
./nb-openmp 5 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "6"
./nb-openmp 6 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "7"
./nb-openmp 7 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "8"
./nb-openmp 8 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "9"
./nb-openmp 9 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE
echo "10"
./nb-openmp 10 1 100 1 samples/test4.txt 0.5 disable -1 -1  2.5 500 >> $FILE



FILE="bh_100_test4_0.1"
echo "$FILE"
echo "1"
./nb-bh 1 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "2"
./nb-bh 2 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "3"
./nb-bh 3 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "4"
./nb-bh 4 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "5"
./nb-bh 5 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "6"
./nb-bh 6 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "7"
./nb-bh 7 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "8"
./nb-bh 8 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "9"
./nb-bh 9 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE
echo "10"
./nb-bh 10 1 100 1 samples/test4.txt 0.1 disable -1 -1  2.5 500 >> $FILE





FILE="bh_100_test4_1"
echo "$FILE"
echo "1"
./nb-bh 1 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "2"
./nb-bh 2 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "3"
./nb-bh 3 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "4"
./nb-bh 4 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "5"
./nb-bh 5 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "6"
./nb-bh 6 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "7"
./nb-bh 7 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "8"
./nb-bh 8 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "9"
./nb-bh 9 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE
echo "10"
./nb-bh 10 1 100 1 samples/test4.txt 1 disable -1 -1  2.5 500 >> $FILE










