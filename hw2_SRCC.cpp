

#include <iostream>
#include <pthread.h>
#include <semaphore.h>
#include <cstdlib>
#include <unistd.h>
#include <vector>
#include <ctime>

#define MAX_WAITING_TIME	1000

sem_t sem_car_capacity;
sem_t sem_free;
pthread_cond_t cond_running;
pthread_mutex_t cont_running_mutex;
pthread_mutex_t rider_list_mutex;
double total_waiting_time = 0;

bool end = false;


using namespace std;

vector<int> rider_list;

struct passenger_arg_t {
	int id;

};

double get_program_time() {
//	static time_t t1 = time(NULL);
//
//	return (time(NULL) - t1) * 1000.0;
	
	static clock_t t1 = clock();
	
	return (clock() - t1) / (double) (CLOCKS_PER_SEC/1000);
}

void * passenger_thread(void * arguments) {
	passenger_arg_t *arg = (passenger_arg_t *) arguments;

	cout << "thread id: " << arg->id << endl;

	clock_t t1, total = 0;
	int count = 0;
	
	int waiting_ms;
	while (!end) {

		waiting_ms = ((rand() % MAX_WAITING_TIME) + 1); // <1;10> miliseconds
		cout << arg->id << " wanders around park for " << waiting_ms << " ms" << endl;
		usleep(waiting_ms);
		cout << arg->id << " returns for the ride" << endl;

		sem_wait(&sem_free);
		if (end) {
			break;
		}
		pthread_mutex_lock(&rider_list_mutex);
		rider_list.push_back(arg->id);
		pthread_mutex_unlock(&rider_list_mutex);

		sem_post(&sem_car_capacity);
		t1 = clock();
		
		//riding
		
		pthread_mutex_lock(&cont_running_mutex);

		pthread_cond_wait(&cond_running, &cont_running_mutex);
		pthread_mutex_unlock(&cont_running_mutex);
		total += clock()-t1;
		count++;
		
	}

	cout << "thread id: " << arg->id << "END" << endl;

	pthread_mutex_lock(&rider_list_mutex);
	total_waiting_time += (total / (double) (CLOCKS_PER_SEC/1000)) / count;
	cout << (total / (double) (CLOCKS_PER_SEC/1000)) / count << endl;
	pthread_mutex_unlock(&rider_list_mutex);

	
	
	return NULL;
}

void car_thread(int time_ms, int N, int car_capacity, int threads) {

	get_program_time();
	int cycle = 0;
	while (cycle < N) {

		cout << "waiting for the cars to be full" << endl;
		for (int i = 0; i < car_capacity; i++) {
			sem_wait(&sem_car_capacity);
		}

		cout << "starting riding at " << get_program_time() << "ms with rider: \"";
		for (vector<int>::iterator it = rider_list.begin(); it != rider_list.end(); it++) {
			cout << *it << " ";
		}
		cout << "\"" << endl;

		usleep(time_ms);
		cout << "finishing riding at " << get_program_time() << "ms" << endl;

		//		for (int i = 0; i < car_capacity; i++) {
		//			pthread_cond_broadcast(&cond_running);
		//		}

		rider_list.clear();
		pthread_cond_broadcast(&cond_running);
		for (int i = 0; i < car_capacity; i++) {
			sem_post(&sem_free);
		}

		cycle++;
	}

	end = true;
	pthread_cond_broadcast(&cond_running);
	for (int i = 0; i < threads; i++) {
		sem_post(&sem_free);
	}


	cout << "end car thread" << endl;
}

int main(int argc, char** argv) {

	int n, C, T, N;

	if (argc != 5) {
		cerr << "Bad number of parameters" << endl;
		return 1;
	}

	n = atoi(argv[1]); // passengers 
	if (n < 2 || n > 10) {
		cerr << "Bad value of n" << endl;
		return 1;
	}

	C = atoi(argv[2]); // capacity of the car
	T = atoi(argv[3]); // time for car going around the track, [ms] 
	N = atoi(argv[4]); // number of simulation steps

	srand(time(NULL));

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	pthread_t *passengers;
	passenger_arg_t *passengers_arg;

	passengers = new pthread_t [n];
	passengers_arg = new passenger_arg_t [n];
	sem_init(&sem_car_capacity, 0, 0);
	sem_init(&sem_free, 0, C);

	pthread_mutex_init(&rider_list_mutex, NULL);
	pthread_mutex_init(&cont_running_mutex, NULL);
	pthread_cond_init(&cond_running, NULL);



	for (int i = 0; i < n; i++) {
		passengers_arg[i].id = i + 1;
		pthread_create(&passengers[i], &attr, (void*(*)(void*))passenger_thread, &passengers_arg[i]);

	}


	car_thread(T, N, C, n);








	for (int i = 0; i < n; i++) {
		pthread_join(passengers[i], NULL);
	}

	cout << "average waiting time: " << total_waiting_time / (double) n << endl;

	pthread_mutex_destroy(&rider_list_mutex);
	pthread_mutex_destroy(&cont_running_mutex);

	sem_destroy(&sem_free);
	sem_destroy(&sem_car_capacity);
	pthread_attr_destroy(&attr);

	delete [] passengers;
	delete [] passengers_arg;

	return 0;

}

