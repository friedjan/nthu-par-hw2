/* 
 * File:   Graphics.cpp
 * Author: fridlik
 * 
 * Created on November 6, 2015, 2:14 PM
 */

#include "Graphics.h"

#include <cstdlib>
#include <unistd.h>

Graphics::Graphics(bool enable, double x_min, double y_min, int width, double axePoint) {
	this->m_width = width;
	this->m_axePoint = axePoint;
	this->m_enable = enable;

	this->m_x_min = x_min;
	this->m_y_min = y_min;

	this->m_pixelsPerUnit = width / axePoint;
	std::cout << "pixelsPerUnit: " << m_pixelsPerUnit << std::endl;

	if (!enable)
		return;

	this->m_display = XOpenDisplay(NULL);
	if (this->m_display == NULL) {
		std::cerr << "cannot open display" << std::endl;
		exit(1);
	}

	
	this->m_screen = DefaultScreen(this->m_display);

	this->m_window = XCreateSimpleWindow(this->m_display, RootWindow(this->m_display, this->m_screen),
			this->X_0, this->Y_0, this->m_width, this->m_width, 1,
			BlackPixel(this->m_display, this->m_screen), BlackPixel(this->m_display, this->m_screen));

	XMapWindow(this->m_display, this->m_window);

	this->m_gc = XCreateGC(this->m_display, this->m_window, 0, NULL);

	XSetForeground(m_display, m_gc, WhitePixel(m_display, m_screen));


	
	sleep(1);

	m_lastRefreshTime = clock();
	refreshCount = 0;
	freq = 0;







}

Graphics::Graphics(const Graphics& orig) {
}

Graphics::~Graphics() {
	if (m_enable) {
		XFreeGC(this->m_display, this->m_gc);
		XCloseDisplay(this->m_display);

	}
}

int Graphics::convertCoordinatesX(double coor) {

	int tmp = (coor - this->m_x_min) * this->m_pixelsPerUnit;
	return tmp;
}

int Graphics::convertCoordinatesY(double coor) {

	int tmp = (coor - this->m_y_min) * this->m_pixelsPerUnit;
	return tmp;
}

void Graphics::drawPoint(Vector &v) {
	if (!m_enable)
		return;
	this->drawPoint(v.coordinate(0), v.coordinate(1));
}

void Graphics::drawPoint(double x, double y) {
	if (!m_enable)
		return;

	//std::cout << "drwPoint - " << convertCoordinatesX(x) << " " << convertCoordinatesY(y) << std::endl;
	XSetForeground(this->m_display, this->m_gc, WhitePixel(this->m_display, this->m_screen));
	XDrawPoint(this->m_display, this->m_window, this->m_gc, convertCoordinatesX(x), convertCoordinatesY(y));
}

void Graphics::drawLine(Vector&v) {
	if (!m_enable)
		return;

	std::cout << "draw x:" << convertCoordinatesX(v.coordinate(0)) << " y:" << convertCoordinatesY(v.coordinate(1)) << std::endl;
	XSetForeground(this->m_display, this->m_gc, WhitePixel(this->m_display, this->m_screen));
	XDrawLine(this->m_display, this->m_window, this->m_gc, convertCoordinatesX(v.coordinate(0)), convertCoordinatesY(v.coordinate(1)), m_width, convertCoordinatesY(v.coordinate(1)));
}

void Graphics::drawSquare(const Borders & b) {
//	if (!m_enable)
//		return;

	XSetForeground(this->m_display, this->m_gc, 0x404040);
	XDrawRectangle(this->m_display, this->m_window, this->m_gc,
			convertCoordinatesX(b.center.coordinate(0) - b.half_size),
			convertCoordinatesY(b.center.coordinate(1) - b.half_size),
			b.half_size * 2.0 * this->m_pixelsPerUnit, b.half_size * 2.0 * this->m_pixelsPerUnit);


}

void Graphics::printBorders(QTree* root) {
	//first draw childs
	if (root == NULL)
		return;
	for (int i = 0; i < 4; i++) {
		printBorders(root->getChild(i));

	}

	//if (root->isPrintable()){
	drawSquare(root->getBorders());
	//}
}

//void Graphics::refresh(Body* bodies, int n) {
//	if (!m_enable)
//		return;
//
//	//clear();
//	for (int i = 0; i < n; i++) {
//		drawPoint(bodies[i].getPosition());
//	}
//	flush();
//	return;
//
//
//	this->refreshCount++;
//
//	//	if (this->refreshCount == 100){
//	//usleep(1000);
//	//if ((clock() - m_lastRefreshTime) / (double) (CLOCKS_PER_SEC / 1000) > 1000.0 / (double) FPS) {
//	//clear();
//
//	for (int i = 0; i < n; i++) {
//		drawPoint(bodies[i].getPosition());
//	}
//	flush();
//
//
//	m_lastRefreshTime = clock();
//	refreshCount = 0;
//	//}
//	//	}
//}

void Graphics::refresh(std::vector<Body *>& v, int colour, bool borders, QTree * root ) {
	if (!m_enable)
		return;
	
	freq++;
	if (freq % 2 != 0){
		
		return;
	}
	

	XClearWindow(this->m_display, this->m_window);
	if (borders)
		printBorders(root);
	for (std::vector<Body *>::iterator it = v.begin(); it != v.end(); it++) {
		XSetForeground(this->m_display, this->m_gc, colour);
		drawPoint((*it)->getPosition());
	}
	
	XFlush(this->m_display);

}

void Graphics::flush() {
	if (!m_enable)
		return;
	XFlush(this->m_display);
}

void Graphics::setForeground(long colour) {
	if (!m_enable)
		return;
	XSetForeground(this->m_display, this->m_gc, colour);
}

void Graphics::clear(double x1, double x2, double y1, double y2) {
	if (!m_enable)
		return;
	//XSetForeground(this->m_display, this->m_gc, WhitePixel(this->m_display, this->m_screen));
	//XDrawRectangle(this->m_display, this->m_window, this->m_gc, 0, 0, this->m_width, this->m_width);
	XClearArea(this->m_display, this->m_window, x1, y1, x2 - x1, y2 - y1, false);
	//XClearWindow(this->m_display, this->m_window);
	//flush();
}