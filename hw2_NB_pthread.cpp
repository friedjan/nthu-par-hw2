#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <vector>
#include <ctime>
#include <cmath>
#include <string>
#include <X11/Xlib.h>
#include <pthread.h>
#include <omp.h>

#include "Body.h"
#include "Graphics.h"

#define G	6.67E-11
#define X_0		0
#define Y_0		0

using namespace std;

vector<Body *> bodies;
Vector *F;
bool enableX;

pthread_barrier_t barier;

struct thread_arg_t {
	int id;
	int threads;
	int T;
	int n;
};

void * working_thread(void * arguments) {
	thread_arg_t *arg = (thread_arg_t *) arguments;

	int i1 = (arg->n / arg->threads) * arg->id + (arg->id < arg->n % arg->threads ? arg->id : arg->n % arg->threads);
	int number_per_thread = arg->n / arg->threads + (arg->id < arg->n % arg->threads ? 1 : 0);


	for (int cycle = 0; cycle < arg->T; cycle++) {

		for (int i = i1; i < i1 + number_per_thread; i++) {
			F[i] = Vector(0, 0);
			for (int j = 0; j < arg->n; j++) {
				if (i != j) {
					F[i] = F[i] + bodies[i]->computeForceTo(bodies[j]);
				}
			}
		}



		if (enableX) {
			pthread_barrier_wait(&barier);
			pthread_barrier_wait(&barier);
		}

		//moving
		for (int i = i1; i < i1 + number_per_thread; i++) {
			bodies[i]->move(F[i]);
		}


		pthread_barrier_wait(&barier);
		pthread_barrier_wait(&barier);

	}



}

int main(int argc, char** argv) {

	cout << 6.67E-11 * 1000 * 1000 << endl;

	int threads, T, theta, Length;
	double mass, t, x_min, y_min, length;

	string filename;


	if (argc != 12 && argc != 8) {
		cerr << "Too few or too much input parameters" << endl;
		exit(1);
	}

	threads = atoi(argv[1]);
	mass = atof(argv[2]);
	T = atoi(argv[3]);
	t = atof(argv[4]);
	filename = string(argv[5]);
	theta = atoi(argv[6]);

	enableX = (string(argv[7]) == string("enable") ? true : false);


	x_min = atof(argv[8]);
	y_min = atof(argv[9]);
	length = atof(argv[10]);
	Length = atoi(argv[11]);

	cout << "enableX " << enableX << endl;
	Graphics graphic(enableX, x_min, y_min, Length, length);


	//	for(double xko = 0; xko < 2; xko += 0.01){
	//		graphic.drawPoint(xko, 1.0);
	//		graphic.flush();
	//	}
	//	
	//	sleep(5);
	//	exit(0);



	ifstream ifd;
	ifd.open(filename.c_str(), ios::in);

	if (!ifd.is_open()) {
		cerr << "cannot open file" << endl;
		exit(2);
	}

	int n;
	ifd >> n;

	F = new Vector[n];


	double x1, x2, v1, v2;
	for (int i = 0; i < n; i++) {
		ifd >> x1 >> x2 >> v1 >> v2;
		//cout << x1 << " " << x2 << " " << v1 << " " << v2 << endl;
		bodies.push_back(new Body(Vector(x1, x2), Vector(v1, v2), mass, t));
	}

	ifd.close();

	if (threads > n)
		threads = n;
	cout << threads << endl;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_t *workers;
	thread_arg_t *workers_arg;

	workers = new pthread_t [threads];
	workers_arg = new thread_arg_t [threads];

	pthread_barrier_init(&barier, NULL, threads + 1);


	double total_time = 0, t1;

	t1 = omp_get_wtime();
	for (int i = 0; i < threads; i++) {
		workers_arg[i].T = T;
		workers_arg[i].id = i;
		workers_arg[i].threads = threads;
		workers_arg[i].n = n;


		pthread_create(&workers[i], &attr, (void*(*)(void*))working_thread, &workers_arg[i]);

	}

	graphic.refresh(bodies, 0xffffff);

	for (int cycle = 0; cycle < T; cycle++) {
		pthread_barrier_wait(&barier);
		total_time += omp_get_wtime() - t1;

		if (enableX) {

			pthread_barrier_wait(&barier);
			pthread_barrier_wait(&barier);

			graphic.refresh(bodies, 0xffffff);
		}
		t1 = omp_get_wtime();
		pthread_barrier_wait(&barier);


	}


	for (int i = 0; i < threads; i++) {
		pthread_join(workers[i], NULL);
	}

	sleep(1);





	for (int i = 0; i < n; i++) {
		delete bodies[i];
	}
	delete [] F;
	delete [] workers;
	delete [] workers_arg;

	pthread_barrier_destroy(&barier);
	pthread_attr_destroy(&attr);

	sleep(1);

	//	/cout << "Total time: " << (double) (total_time / (double) (CLOCKS_PER_SEC/1000)) << " ms"<< endl;
	cout << "Total time: " << total_time << endl;

	return 0;

}

