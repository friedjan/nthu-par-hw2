
#include <iostream>
#include <pthread.h>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <string>
#include <omp.h>

#include "QTree.h"
#include "Graphics.h"
#include "Vector.h"

using namespace std;


QTree *root;
std::vector<Body*> bodies;
std::vector<Body*>::iterator bodies_it;
Vector *F;
pthread_mutex_t mutex_bodies;
pthread_barrier_t barrier;

struct worker_parameter_t {
	int id;
	int threads;
	int T;
	int n;
	double theta;

};

void buildTree() {

	while (true) {
		Body *b;
		pthread_mutex_lock(&mutex_bodies);
		if (bodies_it != bodies.end()) {
			b = *bodies_it;
			bodies_it++;
			pthread_mutex_unlock(&mutex_bodies);
		} else {
			pthread_mutex_unlock(&mutex_bodies);
			break;
		}


		root->insert(b);
	}
}

void * worker_thread(void * arguments) {
	worker_parameter_t *arg = (worker_parameter_t *) arguments;

	int i1 = (arg->n/arg->threads) * arg->id + (arg->id < arg->n % arg->threads? arg->id : arg->n % arg->threads);
	int number_per_thread = arg->n/arg->threads + (arg->id < arg->n % arg->threads ? 1: 0);

	cout << arg->id << " i1:" << i1 << " npt:" << number_per_thread << endl;
	
	
	for (int cycle = 0; cycle < arg->T; cycle++) {
		//create root
		pthread_barrier_wait(&barrier);

		buildTree();
		pthread_barrier_wait(&barrier);
//compute center of mass
		pthread_barrier_wait(&barrier);

		for (int i = i1; i < i1 + number_per_thread; i++) {
			F[i] = root->computeForce(bodies[i], arg->theta);
			//cout << "thread:" << arg->id << " " << F[i] << endl;

		}

		pthread_barrier_wait(&barrier);
		for (int i = i1; i < i1 + number_per_thread; i++) {
			bodies[i]->move(F[i]);
		}
		
		pthread_barrier_wait(&barrier);

		
		
	}
	delete arg;
	
	return 0;
}

void find_borders(double & xmin, double & xmax, double & ymin, double & ymax, double & lenght) {

	xmin = bodies.front()->getX();
	xmax = bodies.front()->getX();
	ymin = bodies.front()->getY();
	ymax = bodies.front()->getY();

	for (std::vector<Body *>::iterator it = bodies.begin(); it != bodies.end(); it++) {
		double x, y;

		x = (*it)->getX();
		y = (*it)->getY();

		if (x < xmin) {
			xmin = x;
		} else if (x > xmax) {
			xmax = x;
		}
		if (y < ymin) {
			ymin = y;
		} else if (y > ymax) {
			ymax = y;
		}
	}
	double x_border_lenght = xmax - xmin;
	double y_border_lenght = ymax - ymin;
	lenght = (x_border_lenght > y_border_lenght) ? x_border_lenght : y_border_lenght;
}

int main(int argc, char** argv) {
	int threads, T, Length;
	bool enableX;
	double mass, t, x_min, y_min, length, theta;
	double compute_force_time = 0, t1, t2, total_time = 0, build_tree_time = 0, io_time = 0;

	string filename;


	if (argc != 12 && argc != 8) {
		cerr << "Too few or too much input parameters" << endl;
		exit(1);
	}

	threads = atoi(argv[1]);
	mass = atof(argv[2]);
	T = atoi(argv[3]);
	t = atof(argv[4]);
	filename = string(argv[5]);
	theta = atof(argv[6]);

	enableX = (string(argv[7]) == string("enable") ? true : false);


	x_min = atof(argv[8]);
	y_min = atof(argv[9]);
	length = atof(argv[10]);
	Length = atoi(argv[11]);

	cout << "enableX " << enableX << endl;
	Graphics graphic(enableX, x_min, y_min, Length, length);

	//graphic.setForeground(0xFFFFFF);
	//graphic.flush();

	t1 = omp_get_wtime();
	ifstream ifd;
	ifd.open(filename.c_str(), ios::in);

	if (!ifd.is_open()) {
		cerr << "cannot open file" << endl;
		exit(2);
	}

	int n;
	ifd >> n;

	bodies = std::vector<Body*>();

	F = new Vector[n];


	double x1, x2, v1, v2;
	double b_x_min, b_x_max;
	double b_y_min, b_y_max;
	double border_lenght;

	
	for (int i = 0; i < n; i++) {
		ifd >> x1 >> x2 >> v1 >> v2;
		bodies.push_back(new Body(Vector(x1, x2), Vector(v1, v2), mass, t));
	}
	ifd.close();

	if (n < threads){
		threads = n;
	}

	pthread_mutex_init(&mutex_bodies, NULL);
	pthread_barrier_init(&barrier, NULL, threads + 1);

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);


	std::vector<pthread_t> threads_arr;
	std::vector<worker_parameter_t> thread_arguments;

	
	for (int i = 0; i < threads; i++) {
		worker_parameter_t *thread_arg = new worker_parameter_t;
		thread_arg->id = i;
		thread_arg->T = T;
		thread_arg->n = n;
		thread_arg->threads = threads;
		thread_arg->theta = theta;
		pthread_t thr;
		threads_arr.push_back(thr);

		pthread_create(&threads_arr.at(i), &attr, (void*(*)(void*))worker_thread, thread_arg);

	}
	io_time = omp_get_wtime() - t1;

	
	for (int cycle = 0; cycle < T; cycle++) {
		t1 = omp_get_wtime();
		find_borders(b_x_min, b_x_max, b_y_min, b_y_max, border_lenght);

		
		root = new QTree(Borders(Vector(b_x_min + (b_x_max - b_x_min) / 2, b_y_min + (b_y_max - b_y_min) / 2), border_lenght / 2));
		bodies_it = bodies.begin();
		//		for(int i = 0; i < n; i++){
		//			F[i] = Vector(0,0);
		//		}
		
		pthread_barrier_wait(&barrier);
		//build tree
		pthread_barrier_wait(&barrier);
		root->computeCenterOfMass();
		build_tree_time += omp_get_wtime() - t1;
		pthread_barrier_wait(&barrier);
		t2 = omp_get_wtime();
		//compute force

		pthread_barrier_wait(&barrier);
		//move
		pthread_barrier_wait(&barrier);
		compute_force_time += omp_get_wtime() - t2;
		
		total_time += omp_get_wtime() - t1;
		graphic.refresh(bodies, 0xffffff, true, root);
		
		delete root;
	}

	for (std::vector<pthread_t>::iterator it = threads_arr.begin(); it != threads_arr.end(); it++) {
		pthread_join(*it, NULL);
	}

	for(int i = 0; i < n; i++){
		delete bodies[i];
	}
	
	sleep(1);
	pthread_mutex_destroy(&mutex_bodies);

	//delete root;
	delete [] F;
	
	
	cout << "io_time: " << io_time << endl;
	cout << "build_tree_time: " << build_tree_time << endl;
	cout << "compute_force_time: "  << compute_force_time << endl;
	cout << "total_time: " << total_time << endl;

	return 0;
}