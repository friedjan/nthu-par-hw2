CXX=g++
CXXFLAGS=-Wall -pedantic -g
LDFLAGS=-lX11 -fopenmp
EXECUTABLE=nb-pthread nb-bh nb-openmp srcc
VALGRIND=valgrind -v --leak-check=full --show-reachable=yes --track-origins=yes
SOURCES=Body.cpp Graphics.cpp QTree.cpp Vector.cpp
RM=rm -f

all: 	nb-pthread \
	nb-bh \
	nb-openmp \
	nb-serial \
	srcc
nb-pthread:
	$(CXX) $(LDFLAGS) -pthread $(SOURCES) hw2_NB_pthread.cpp -o nb-pthread
nb-bh:
	$(CXX) $(LDFLAGS) -pthread $(SOURCES) hw2_NB_BHalgo.cpp -o nb-bh
nb-openmp:
	$(CXX) $(LDFLAGS) $(SOURCES) hw2_NB_openmp.cpp -o nb-openmp
nb-serial:
	$(CXX) $(LDFLAGS) $(SOURCES) hw2_NB_serial.cpp -o nb-serial

srcc:
	$(CXX) -pthread hw2_SRCC.cpp -o srcc

 
clean:
	$(RM) $(SOURCES:.cpp=.o) hw2_NB_pthread.o hw2_NB_BHalgo.o hw2_NB_openmp.o hw2_SRCC.o  $(EXECUTABLE)
 
