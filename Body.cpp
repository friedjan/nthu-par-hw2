/* 
 * File:   Body.cpp
 * Author: fridlik
 * 
 * Created on November 5, 2015, 11:00 PM
 */

#include "Body.h"

#include <iostream>
#include <cmath>

Body::Body(){
	
}

Body::Body(Vector position, Vector velocity, double mass, double time) {
	this->m_position = position;
	this->m_velocity = velocity;
	this->m_mass = mass;
	this->m_time = time;
}

Body::Body(const Body& orig) {
	this->m_position = orig.m_position;
	this->m_velocity = orig.m_velocity;
	this->m_mass = orig.m_mass;
}

Body::~Body() {
}

void Body::move(Vector& F){
	m_velocity = m_velocity + ((F * m_time) / m_mass);
	m_position = m_position + (m_velocity * m_time);
}

std::ostream & operator << ( std::ostream & os, const Body & b){
	os << " pos: " << b.m_position << "  vel: " << b.m_velocity;
	return os;
}

Vector Body::computeForceTo(Body &b){
	return this->computeForceTo(b.m_position, b.m_mass);
}

Vector Body::computeForceTo(Body *b){
	return this->computeForceTo(b->m_position, b->m_mass);
}


Vector Body::computeForceTo(Vector & v, double mass){
	double distance = this->m_position.distanceTo(v);
	double force = (G * this->m_mass * mass) / (pow(distance, 3.0) + EPS);
	//double force = (G * this->m_mass * this->m_mass) / pow(distance, 3);
	
	//return force;
	return ((v - this->m_position ) ) * force;
}

Vector & Body::getPosition(){
	return this->m_position;
	//g.drawLine(this->m_position);
}