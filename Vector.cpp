/* 
 * File:   Vector.cpp
 * Author: fridlik
 * 
 * Created on November 5, 2015, 10:26 PM
 */

#include "Vector.h"
#include <cmath>
#include <iomanip>

Vector::Vector() {
	this->m_data[0] = 0;
	this->m_data[1] = 0;
}

Vector::Vector(double x1, double x2) {
	this->m_data[0] = x1;
	this->m_data[1] = x2;
}

Vector::Vector(const Vector& orig) {
	
	
	this->m_data[0] = orig.m_data[0];
	this->m_data[1] = orig.m_data[1];
	
}

Vector::~Vector() {
	
}

Vector & Vector::operator=(const Vector&b) {
	if (&b == this)
		return *this;
	
	this->m_data[0] = b.m_data[0];
	this->m_data[1] = b.m_data[1];
	return *this;
}

Vector Vector::operator+(const Vector&b) {
	Vector tmp;

	tmp.m_data[0] = this->m_data[0] + b.m_data[0];
	tmp.m_data[1] = this->m_data[1] + b.m_data[1];
	return tmp;
}

Vector Vector::operator-(const Vector&b) {
	Vector tmp;

	tmp.m_data[0] = this->m_data[0] - b.m_data[0];
	tmp.m_data[1] = this->m_data[1] - b.m_data[1];
	return tmp;
}

Vector Vector::operator*(double b) {
	Vector tmp;

	tmp.m_data[0] = this->m_data[0] * b;
	tmp.m_data[1] = this->m_data[1] * b;
	return tmp;
}

Vector Vector::operator/(double b) {
	Vector tmp;

	tmp.m_data[0] = this->m_data[0] / b;
	tmp.m_data[1] = this->m_data[1] / b;
	return tmp;
}

std::ostream & operator << ( std::ostream & os, const Vector & v){
	std::cout << std::setprecision(12);
	os << "x:" << v.m_data[0] << " y:" << v.m_data[1];
	return os;
}


double Vector::distanceTo(const Vector& v){
	Vector tmp = *this - v;
	
	return sqrt((tmp.m_data[0] * tmp.m_data[0]) + (tmp.m_data[1] * tmp.m_data[1]));
	
	
//	return sqrt(pow(this->m_data[0] - v.m_data[0], 2) + pow(this->m_data[1] - v.m_data[1], 2));
	
}

Vector Vector::direction(){
	Vector tmp = *this * (1.0 / sqrt(this->m_data[0] * this->m_data[0] + this->m_data[1] * this->m_data[1]));
	return tmp;
}

double Vector::coordinate(int x) const{
	if (x < 0 || x > 1){
		std::cerr << "bad axe range. Must be [0,1]" << std::endl;
	}
	return this->m_data[x];
}
