#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <vector>
#include <ctime>
#include <cmath>
#include <string>
#include <X11/Xlib.h>
#include <omp.h>

#include "Body.h"
#include "Graphics.h"

#define G	6.67E-11
#define X_0		0
#define Y_0		0

using namespace std;

int main(int argc, char** argv) {

	cout << 6.67E-11 * 1000 * 1000 << endl;

	int threads, T, theta, Length;
	bool enableX;
	double mass, t, x_min, y_min, length;

	string filename;


	if (argc != 12 && argc != 8) {
		cerr << "Too few or too much input parameters" << endl;
		exit(1);
	}

	threads = atoi(argv[1]);
	mass = atof(argv[2]);
	T = atoi(argv[3]);
	t = atof(argv[4]);
	filename = string(argv[5]);
	theta = atoi(argv[6]);

	enableX = (string(argv[7]) == string("enable") ? true : false);


	x_min = atof(argv[8]);
	y_min = atof(argv[9]);
	length = atof(argv[10]);
	Length = atoi(argv[11]);

	cout << "enableX " << enableX << endl;
	Graphics graphic(enableX, x_min, y_min, Length, length);

	graphic.setForeground(0xFFFFFF);
	graphic.flush();


	//	for(double xko = 0; xko < 2; xko += 0.01){
	//		graphic.drawPoint(xko, 1.0);
	//		graphic.flush();
	//	}
	//	
	//	sleep(5);
	//	exit(0);



	ifstream ifd;
	ifd.open(filename.c_str(), ios::in);

	if (!ifd.is_open()) {
		cerr << "cannot open file" << endl;
		exit(2);
	}

	int n;
	ifd >> n;

	vector<Body *> bodies;
	//bodies = new Body[n];

	Vector *F = new Vector[n];


	double x1, x2, v1, v2;
	for (int i = 0; i < n; i++) {
		ifd >> x1 >> x2 >> v1 >> v2;
		//cout << x1 << " " << x2 << " " << v1 << " " << v2 << endl;
		bodies.push_back(new Body(Vector(x1, x2), Vector(v1, v2), mass, t));
	}

	ifd.close();
	cout << threads << endl;
	//graphic.refresh(bodies, n);
	sleep(1);
	//exit(1);

	omp_set_num_threads(threads);
	double total_time = 0, t1;

	for (int cycle = 0; cycle < T; cycle++) {
		t1 = omp_get_wtime();
#pragma omp parallel for
		for (int i = 0; i < n; i++) {
			F[i] = Vector(0, 0);
		}

		int i, j;
#pragma omp parallel for collapse(2) private(i,j)
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if (i != j) {
					F[i] = F[i] + bodies[i]->computeForceTo(bodies[j]);
				}
			}
		}

#pragma omp parallel for
		for (int i = 0; i < n; i++) {
			bodies[i]->move(F[i]);
		}

		total_time += omp_get_wtime() - t1;

		if (enableX) {
			graphic.refresh(bodies, 0xffffff);
		}

	}



	for (int i = 0; i < n; i++) {
		delete bodies[i];
	}

	delete [] F;

	sleep(1);

	cout << "Total time: " << total_time << endl;

	return 0;

}

