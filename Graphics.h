/* 
 * File:   Graphics.h
 * Author: fridlik
 *
 * Created on November 6, 2015, 2:14 PM
 */

#ifndef GRAPHICS_H
#define	GRAPHICS_H

#define FPS 600

#include <X11/Xlib.h>
#include <X11/extensions/Xdbe.h>
#include <X11/Xutil.h>
#include "Vector.h"
#include "Body.h"
#include "QTree.h"

class Graphics {
public:
	Graphics(bool enable, double x_min, double y_min, int width, double axePoint);
	Graphics(const Graphics& orig);
	virtual ~Graphics();
	
	int convertCoordinatesX(double);
	int convertCoordinatesY(double);
	
	void drawPoint(Vector &);
	void drawPoint(double x, double y);
	void drawLine(Vector &);
	void drawSquare(const Borders & );
	
	void setForeground(long colour);
	
//	void refresh(Body * bodies, int n);
	void refresh(std::vector<Body *> &, int colour, bool borders = false, QTree * root = NULL);
	void printBorders(QTree * root);
	void flush();
	void clear(double x1, double x2, double y1, double y2);
	
private:
	Window m_window;
	Display *m_display;
	GC m_gc;
	XdbeBackBuffer m_backBuffer;
	Visual *d_vis;
	int m_screen;
	int m_width;
	int m_axePoint;
	bool m_enable;
	const static int Y_0 = 0;
	const static int X_0 = 0;
	
	int m_pixelsPerUnit;
	double m_x_min;
	double m_y_min;
	clock_t m_lastRefreshTime;
	int refreshCount;
	int freq;
	
};

#endif	/* GRAPHICS_H */

