/* 
 * File:   QTree.h
 * Author: fridlik
 *
 * Created on November 11, 2015, 5:03 PM
 */

#ifndef QTREE_H
#define	QTREE_H

#define EPSILON	0.000001

#include <cmath>
#include <vector>
#include <pthread.h>
#include "Body.h"

struct Borders {
	Vector center;
	double half_size;

	Borders(Vector center = Vector(), double half_size = 0) {
		this->center = center;
		this->half_size = half_size;
	}

	bool double_compare(double a, double b) {
		return std::abs(a - b) < EPSILON;
	}

	bool contains(double x, double y) {
		if ((center.coordinate(0) - half_size < x || double_compare(center.coordinate(0) - half_size, x)) &&
				(center.coordinate(0) + half_size > x || double_compare(center.coordinate(0) + half_size, x))) {
			if ((center.coordinate(1) - half_size < y || double_compare(center.coordinate(1) - half_size, y)) &&
					(center.coordinate(1) + half_size > y || double_compare(center.coordinate(1) + half_size, y))) {
				return true;
			}
		}
		return false;
	}
};

class QTree {
public:
	QTree();
	QTree(Borders);
	QTree(const QTree& orig);
	virtual ~QTree();
	void divide();
	bool insert(Body *, bool lock = true);
	friend std::ostream & operator<<(std::ostream &, const QTree &);
	void print(std::ostream &, int spaces) const;

	bool isLeaf()const {
		return leaf;
	}

	bool isPrintable()const {
		return ( !leaf && nw != NULL);
	}

	const Borders & getBorders() const {
		return borders;
	}
	QTree * getChild(int c);

	Vector & getCenterOfMass();

	Vector computeForce(Body *, double theta);

	void computeCenterOfMass();
	
	int countItem();

private:
	std::vector<Body> bodies;
	Borders borders;
	bool leaf;
	Body * item;
	QTree *nw;
	QTree *ne;
	QTree *sw;
	QTree *se;
	pthread_mutex_t m_lock;
	double total_mass;
	double center_of_mass_acc[2];
	long childs;
	Vector force;
	Vector mass_center;

};

#endif	/* QTREE_H */

