/* 
 * File:   Vector.h
 * Author: fridlik
 *
 * Created on November 5, 2015, 10:26 PM
 */

#ifndef VECTOR_H
#define	VECTOR_H

#define G	6.67E-11
#define EPS 0.000001

#include <iostream>

class Vector {
public:
	Vector();
	Vector(double x1, double x2);
	Vector(const Vector& orig);
	virtual ~Vector();
	
	Vector & operator=(const Vector&);
	Vector operator+(const Vector &);
	Vector operator-(const Vector &);
	Vector operator*(double);
	Vector operator/(double);
	Vector direction();
	
	double distanceTo(const Vector &);
	
	double coordinate(int x) const;
	
	friend  std::ostream & operator << ( std::ostream & , const Vector & );
	
private:
	double m_data[2];
	
};

#endif	/* VECTOR_H */

